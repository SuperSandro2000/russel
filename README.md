Worker - Russel
------------------

Linux Daemon for distributing Tasks through a decentralised P2P network.

**Contact**: <revol-xut@protonmail.com>

## Compile
```bash

   $ mkdir build & cd build

   $ cmake .. -D_UNITTESTS=OFF

   $ make
```

## Install

Root permissions are required to put the service and config files into secure locations like /etc/

```bash

    $ sudo make install

```

## Run the Daemon

```bash

    $ ./daemon/russel

```

Systemd support will come soon.

## Change Log

**v0.1c** (2019.12.12)

+ Added auto connect
+ Added basic network interface (e.g Heartbeat)
+ hardened listeners
+ Removed pre-allocated socket buffer
+ Improved cmake for testing purposes

**v0.1b** (2019.12.6)

+ Added AF_INET and AF_UNIX (IPC) Sockets
+ Added Socket Manager
+ Added proper installation
**Note**: The Daemon currently just sends back what he received


**v0.1a** (2019.11.22)

+ Added Logger, Config reader and handler
+ Added Runtime Object
