//
// Created by revol-xut on 11/9/19.
//

#include "ipc_server.hpp"
#include "../globals.hpp"

IPCServer::IPCServer(const std::string &sockPath) {

    if ((thisSocket = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
        runtime->getLogger(ERROR) << "IPC SocketInterface could not be created";
        runtime->terminate();
        return;
    }
    runtime->getLogger(INFO) << "IPC SocketInterface was created interface at: " + sockPath;

    memset(&address, 0, sizeof(address));
    address.sun_family = AF_UNIX;
    strncpy(address.sun_path, sockPath.c_str(), sizeof(address.sun_path) - 1);
    unlink(sockPath.c_str());
    bindToAddr();
}

IPCServer::IPCServer() {

    const std::string sockFileDesc = runtime->getNetworkConfig()->getString("ipc");

    if ((thisSocket = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
        runtime->getLogger(FATAL) << "IPC SocketInterface could not be created";
        runtime->terminate();
    } else {
        runtime->getLogger(INFO) << "IPC SocketInterface was created";
    }
    runtime->getLogger(INFO) << "IPC SocketInterface at: " + sockFileDesc;

    memset(&address, 0, sizeof(address));
    address.sun_family = AF_UNIX;

    strncpy(address.sun_path, sockFileDesc.c_str(), sizeof(address.sun_path) - 1);
    unlink(sockFileDesc.c_str());
    bindToAddr();

}

IPCServer::~IPCServer() = default;

void IPCServer::listenConnection() {
    if (listen(thisSocket, connectionIPCCache) < 0) {
        runtime->getLogger(FATAL) << "Listening on IPC Socket failed";
    }
}

auto IPCServer::acceptConnection() -> std::shared_ptr<Connection> {

    socklen_t addrlen = sizeof(struct sockaddr_in);
    char str[INET_ADDRSTRLEN];

    int new_socket = accept(thisSocket, (struct sockaddr *) &address, &addrlen);
    if (new_socket > 0) {
        runtime->getLogger(INFO) << "IPC incoming connection";
    }

    sockets.push_back(new_socket);
    valid.insert(std::pair<int, bool>(new_socket, true));
    inet_ntop(AF_INET, &(address), str, INET_ADDRSTRLEN);

    auto connection = std::make_shared<Connection>();

    connection->socket = new_socket;
    connection->address = std::string(str);

    return connection;
}

auto IPCServer::bindToAddr() -> int {
    if (bind(thisSocket,
             (struct sockaddr *) &address,
             sizeof(address)) == -1) {
        runtime->getLogger(ERROR) << "IPC SocketInterface could not be bind too this address";
        return 1;
    }
    return 0;
}