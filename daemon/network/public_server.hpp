//
// Created by revol-xut on 11/23/19.
//

#ifndef RUSSEL_PUBLIC_SERVER_HPP
#define RUSSEL_PUBLIC_SERVER_HPP

#include "socket.hpp"

constexpr int connectionPubCache = 10;

class PublicServer : public SocketInterface {
public:
    /*!
     * @brief Uses default values from config ('host', 'port') to create the exposed server socket
     */
    PublicServer();

    /*!
     * @brief Uses given values to create the exposed server socket
     * @param ip Ipv4 Address
     * @param port Port
     */
    explicit PublicServer(const std::string &ip, unsigned short port);

    /*!
     * @brief Default destructor
     */
    ~PublicServer();

    /*!
     * @brief Listens for incoming connections and accepts them.
     * @return Returns struct with all relevant information
     */
    auto acceptConnection() -> std::shared_ptr<Connection> override;

    void listenConnection() override;

private:
    /*!
     * @brief Bind Socket too defined values
     * @return success status 0 or failed 1
     */
    auto bindToAddr() -> int override;

    auto configure(const std::string &ip, unsigned short port) -> int;


    const int opt = 1;
    struct sockaddr_in address{};
    const int addrlen = sizeof(address);


};


#endif //RUSSEL_PUBLIC_SERVER_HPP
