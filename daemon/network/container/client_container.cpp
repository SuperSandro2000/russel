//
// Created by worker on 12/7/19.
//

#include "client_container.hpp"
#include "../../globals.hpp"

ClientContainer::ClientContainer() {
    setIdentity(CLIENT);
    FD_ZERO(&socketSet);
    // Adds the two file descriptors to the set

}

ClientContainer::~ClientContainer() = default;


auto ClientContainer::connectToEngine(const std::string &host, unsigned short port) -> int {
    auto ptr = std::make_shared<Client>();
    auto con = std::make_shared<Connection>();
    int status = ptr->connectViaHost(host, port, con);

    if (status != 0) {
        runtime->getLogger(DEBUG) << "Client Container could not connect to: " + host + ":" + std::to_string(port);
        return -1;
    }
    FD_SET(con->socket, &socketSet);

    appendConnection(con);
    otherEngines.push_back(ptr);

    return 0;
}

auto ClientContainer::getClient(unsigned index) -> const std::shared_ptr<Client> & {
    return otherEngines.at(index);
}

auto ClientContainer::getSize() -> unsigned {
    return otherEngines.size();
}