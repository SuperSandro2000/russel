//
// Created by revol-xut on 12/7/19.
//

#include "server_container.hpp"


ServerContainer::ServerContainer(bool ipc) {
    //connections = {};
    /*
     * Uses default values from config
     */
    if (ipc) {
        setSocket(std::make_unique<IPCServer>());
        setIdentity(IPC);
    } else {
        setSocket(std::make_unique<PublicServer>());
        setIdentity(PUBLIC);
    }
    auto sock = getSocket();
    sockMax = sock->getSocket();

    FD_ZERO(&socketSet);
    // Adds the two file descriptors to the set
    FD_SET(sock->getSocket(), &socketSet);

    sock->listenConnection();

}

ServerContainer::~ServerContainer() = default;


void ServerContainer::checkConnections() {
    timeval timeout{0, 500};

    // Updates set
    socketRead = socketSet;
    int iterations = 0; // Security Mechanism

    for (;;) {
        int ready = select(sockMax + 1, &socketRead, nullptr, nullptr, &timeout);
        //todo: Check if FD_SETSIZE is relevant if this is the case we need more sets too handle the connections

        if (FD_ISSET(getSocket()->getSocket(), &socketRead)) {
            // Checks for new incoming connection
            auto connection = getSocket()->acceptConnection();

            // Adds new connection too the socket set
            FD_SET(connection->socket, &socketSet);
            if (connection->socket > sockMax) {
                sockMax = connection->socket;
            }
            socketCount++;

            appendConnection(connection);
        }
        if (ready <= 0 or ready - iterations <= 0) {
            break;
        }
        iterations++;
    }
}