//
// Created by revol-xut on 12/7/19.
//

#ifndef RUSSEL_SERVER_CONTAINER_HPP
#define RUSSEL_SERVER_CONTAINER_HPP

#include "../socket_handler.hpp"

class ServerContainer : public SocketWatcher {
public:

    /*!
     * @brief Creates the Public and Unix Domain socket
     */
    explicit ServerContainer(bool);

    /*!
     * @brief Default destructor
     */
    ~ServerContainer();


    /*!
     * @brief Checks for new incoming connections and accepts them
     */
    void checkConnections();


private:
    int sockMax = 0;
    int socketCount = 0;
};


#endif //RUSSEL_SERVER_CONTAINER_HPP
