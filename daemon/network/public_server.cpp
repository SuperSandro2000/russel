//
// Created by revol-xut on 11/23/19.
//

#include "public_server.hpp"
#include "../globals.hpp"

PublicServer::PublicServer(const std::string &ip, unsigned short port) {
    if (configure(ip, port) != 0) {
        runtime->getLogger(FATAL) << "Failed to setup public Socket";
    }
}

PublicServer::PublicServer() {
    if (configure(runtime->getNetworkConfig()->getString("host"),
                  (unsigned short) (runtime->getNetworkConfig()->getInteger("port"))) != 0) {
        runtime->getLogger(FATAL) << "Failed to setup public Socket";
    }
}

PublicServer::~PublicServer() = default;

auto PublicServer::configure(const std::string &ip, unsigned short port) -> int {
    if ((thisSocket = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        runtime->getLogger(FATAL) << "Public SocketInterface could not be created";
        runtime->terminate();
        return 1;
    }

    runtime->getLogger(INFO) << "Public SocketInterface was created";
    valid = {};
    sockets = {};

    address.sin_family = AF_INET;
    inet_pton(AF_INET, ip.c_str(), &address.sin_addr);
    address.sin_port = htons(port);

    if (bindToAddr() == 0) {
        runtime->getLogger(INFO) << "Public Socket bind was successful";
    } else {
        return 1;
    }
    return 0;
}


auto PublicServer::bindToAddr() -> int {

    if (setsockopt(thisSocket, SOL_SOCKET,
                   SO_REUSEADDR | SO_REUSEPORT, &opt,
                   sizeof(opt))) {
        runtime->getLogger(FATAL) << "setsockopt failed";
        runtime->terminate();
        return 1;
    }

    if (bind(thisSocket, (struct sockaddr *) &address,
             sizeof(address)) < 0) {
        runtime->getLogger(FATAL) << "bind failed";
        runtime->terminate();
        return 1;
    }
    return 0;
}

void PublicServer::listenConnection() {
    if (listen(thisSocket, connectionPubCache) < 0) {
        runtime->getLogger(FATAL) << "Listening on public Socket failed";
    }
}

auto PublicServer::acceptConnection() -> std::shared_ptr<Connection> {

    char str[INET_ADDRSTRLEN];
    int newSocket = 0;
    if ((newSocket = accept(
            thisSocket, (struct sockaddr *) &address, (socklen_t *) &addrlen)) < 0) {
        std::cerr << "Accept on public Socket failed" << std::endl;
    }
    sockets.push_back(newSocket);
    valid.insert(std::pair<int, bool>(newSocket, true));

    inet_ntop(AF_INET, &(address), str, INET_ADDRSTRLEN);

    auto connection = std::make_shared<Connection>();

    connection->socket = newSocket;
    connection->address = std::string(str);

    return connection;

}
