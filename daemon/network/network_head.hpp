//
// Created by revol-xut on 12/8/19.
//

#ifndef RUSSEL_NETWORK_HEAD_HPP
#define RUSSEL_NETWORK_HEAD_HPP


#include "socket_handler.hpp"
#include "../config/config.hpp"
#include "container/client_container.hpp"
#include "container/server_container.hpp"


/*!
 * @brief This class manages all communication services (IPC Server, Pub Server, and client connections to other russel services.
 */
class NetworkHead {
public:

    NetworkHead();

    NetworkHead(bool ipc, bool pub, bool clients);

    ~NetworkHead();

    void checkEveryThing(const std::shared_ptr<MessageList> &messages);

    void send(const std::shared_ptr<MessageList> &messages);

private:

    void autoConnect();

    // Marks if the communication way is enabled
    bool ipc = false, pub = false, cli = false;
    std::shared_ptr<ServerContainer> serverIpc;
    std::shared_ptr<ServerContainer> serverPub;
    std::shared_ptr<ClientContainer> clientPub;

};


#endif //RUSSEL_NETWORK_HEAD_HPP
