//
// Created by revol-xut on 11/9/19.
//

#ifndef WORKER_IPC_SERVER_HPP
#define WORKER_IPC_SERVER_HPP

#include "socket.hpp"

constexpr int connectionIPCCache = 5;

class IPCServer : public SocketInterface {
public:
    /*!
     * @brief Uses default 'ipc' value from config to create the Unix Domain Socket
     */
    explicit IPCServer();

    /*!
     * @brief Uses given value to create the Unix Domain Socket
     * @param sockPath
     */
    explicit IPCServer(const std::string &sockPath);

    /*!
     * @brief default destructor
     */
    ~IPCServer();

    /*!
     * @brief Listens for incoming connections and accepts them.
     * @return Returns struct with all relevant information
     */
    auto acceptConnection() -> std::shared_ptr<Connection> override;

    void listenConnection() override;

private:

    /*!
     * @brief Bind Socket too defined values
     * @return success status 0 or failed 1
     */
    auto bindToAddr() -> int override;

    struct sockaddr_un address{};

};


#endif //WORKER_IPC_SERVER_HPP
