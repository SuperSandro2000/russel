//
// Created by worker on 12/7/19.
//

#include "client.hpp"
#include "../globals.hpp"


Client::Client() {
    if ((thisSocket = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        runtime->getLogger(FATAL) << "Failed to setup client Socket";
    }
    serv_addr = {};
}

Client::~Client() = default;


auto
Client::connectViaHost(const std::string &host, unsigned short port, const std::shared_ptr<Connection> &con) -> int {
    return connectViaHost(host, port, con.get());
}

auto Client::connectViaHost(const std::string &host, unsigned short port, Connection *con) -> int {

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(port);

    if (inet_pton(AF_INET, host.c_str(), &serv_addr.sin_addr) <= 0) {
        runtime->getLogger(ERROR) << "Invalid address / Address not unknown";
        return -1;
    }

    if (connect(thisSocket, (struct sockaddr *) &serv_addr,
                sizeof(serv_addr)) < 0) {
        runtime->getLogger(ERROR) << "Connection to other server failed";
        return -1;
    }
    con->socket = thisSocket;
    con->address = host + ":" + std::to_string(port);
    con->ignore = false;
    con->clientObj = this;

    return 0;
}

auto Client::acceptConnection() -> std::shared_ptr<Connection> {
    return nullptr;
}

auto Client::bindToAddr() -> int {
    return -1;
}

auto Client::listenConnection() -> void {}