//
// Created by worker on 12/7/19.
//

#ifndef RUSSEL_CLIENT_HPP
#define RUSSEL_CLIENT_HPP

#include "socket.hpp"


class Client : public SocketInterface {
public:

    /*!
     * Creates Client object
     */
    explicit Client();

    /*!
     * Destructor
     */
    ~Client();

    /*!
     * @brief Will try to connect to given address will push information on provided raw pointer
     * @param host IP Address
     * @param port Port
     * @param con  Raw pointer too connection object
     * @return status 0 ok -1 failed
     */
    auto connectViaHost(const std::string &host, unsigned short port, Connection *con) -> int;

    /*!
     * @brief Will try to connect to given address will push information on provided smart pointer
     * @param host IP Address
     * @param port Port
     * @param con Smart pointer too Connection object
     * @return status 0 ok -1 failed
     */
    auto connectViaHost(const std::string &host, unsigned short port, const std::shared_ptr<Connection> &con) -> int;

    auto listenConnection() -> void override;

    auto acceptConnection() -> std::shared_ptr<Connection> override;

    auto bindToAddr() -> int override;

private:

    struct sockaddr_in serv_addr{};
};

#endif //RUSSEL_CLIENT_HPP
