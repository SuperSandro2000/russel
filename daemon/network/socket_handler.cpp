//
// Created by revol-xut on 11/23/19.
//

#include <functional>

#include "../globals.hpp"
#include "socket_handler.hpp"

void SocketWatcher::checkReceiving(const std::shared_ptr<MessageList> &ptr) {

    socketRead = socketSet;
    unsigned i = 0;
    std::shared_ptr<RawMessage> message;

    for (const std::shared_ptr<Connection> &connection : connections) {
        if (!connection->ignore && FD_ISSET(connection->socket, &socketRead)) {

            message = std::make_shared<RawMessage>();
            mySocket->receiveSmartPtrByte(message, connection->socket);
            ptr->messages.push_back(message);
            ptr->messageCount++;
            message->socket = connection->socket;
            message->origin = identity;

            if (!mySocket->checkValidity(connection->socket)) {
                FD_CLR(connection->socket, &socketSet); // deletes socket from set
                eraseSocket(i); // deletes corresponding connection
                i--; //decreases iterator
            }
        }
        i++;
    }
}

void SocketWatcher::eraseSocket(unsigned connectionIndex) {
    if (connectionIndex >= connections.size()) {
        mySocket->closeSocket(connections[connectionIndex]->socket);
        connections.erase(connections.begin() + connectionIndex);
    } else {
        runtime->getLogger(ERROR) << "SocketHandler just wanted to destroy an non existing socket";
    }
}

void SocketWatcher::routeAndSend(const std::shared_ptr<RawMessage> &message) {
    mySocket->sendSmartPointer(message);
}

void SocketWatcher::routeAndSend(const RawMessage *message) {
    mySocket->sendRawMessage(*message);
}

void SocketWatcher::appendConnection(const std::shared_ptr<Connection> &connection) {
    connections.push_back(connection);
}

void SocketWatcher::setIdentity(unsigned short id) {
    identity = id;
}

auto SocketWatcher::getSocket() -> std::shared_ptr<SocketInterface> {
    return mySocket;
}

void SocketWatcher::setSocket(const std::shared_ptr<SocketInterface> &ptr) {
    mySocket = ptr;
}