//
// Created by revol-xut on 11/23/19.
//

#ifndef RUSSEL_SOCKET_HANDLER_HPP
#define RUSSEL_SOCKET_HANDLER_HPP

#include <memory>
#include <vector>

#include "ipc_server.hpp"
#include "public_server.hpp"
#include "socket.hpp"

constexpr int IPC = 0;
constexpr int PUBLIC = 1;
constexpr int CLIENT = 2;

struct MessageList {
    unsigned messageCount;
    std::vector<std::shared_ptr<RawMessage>> messages;
};

class SocketWatcher {
public:
    /*!
     * @brief Checks all registered sockets if the received something and closes invalid ones
     * @param ptr Pointer where the data should be put on
     */
    void checkReceiving(const std::shared_ptr<MessageList> &ptr);

    /*!
     * @brief Sends message too the client
     * @param message
     */
    void routeAndSend(const std::shared_ptr<RawMessage> &message);

    /*!
     * @brief Sends message too the client
     */
    void routeAndSend(const RawMessage *message);

    /*!
     * @brief Appends given connection to the connection list
     */
    void appendConnection(const std::shared_ptr<Connection> &connection);

    /*!
     * @brief Sets identity value which tell what kind of function this object currently provides (ipc, pub,client)
     */
    void setIdentity(unsigned short id);

    /*!
     * @brief Returns pointer to the handled socket
     * @return socket pointer
     */
    auto getSocket() -> std::shared_ptr<SocketInterface>;

    /*!
     * @brief Sets the socket which should be managed by this object
     * @param ptr
     */
    void setSocket(const std::shared_ptr<SocketInterface> &ptr);

    /*!
     * @brief Deletes socket
     * @param connectionIndex index in the client list
     */
    void eraseSocket(unsigned connectionIndex);

private:

    std::shared_ptr<SocketInterface> mySocket;
    std::vector<std::shared_ptr<Connection>> connections; // All connected connections including other ipc sockets

    unsigned short identity;

protected:

    fd_set socketSet, socketRead;

};


#endif //RUSSEL_SOCKET_HANDLER_HPP
