//
// Created by revol-xut on 11/9/19.
//
#include <cmath>
#include <cstring>
#include <iostream>

#include "socket.hpp"

using uchar = unsigned char;

void createHeader(uint32_t size, char *return_value) {
    return_value[0] = *((uchar *) (&size)); //NOLINT
    return_value[1] = *((uchar *) (&size) + 1); //NOLINT
    return_value[2] = *((uchar *) (&size) + 2); //NOLINT
    return_value[3] = *((uchar *) (&size) + 3); //NOLINT
}

auto unpackHeader(const char *data) -> uint32_t {
    uint32_t output;

    *((uchar *) (&output)) = (uchar) data[0]; //NOLINT
    *((uchar *) (&output) + 1) = (uchar) data[1]; //NOLINT
    *((uchar *) (&output) + 2) = (uchar) data[2]; //NOLINT
    *((uchar *) (&output) + 3) = (uchar) data[3]; //NOLINT

    return output;
}

auto SocketInterface::sendString(const std::string &message, int socket) -> int {

    auto *rawMessage = new RawMessage{};

    rawMessage->message = message.c_str();
    rawMessage->size = message.size();
    rawMessage->socket = socket;

    auto status = sendByte(rawMessage);

    delete rawMessage;
    return status;
}

auto SocketInterface::sendRawMessage(const RawMessage &message) -> int {
    return sendByte(&message);
}

auto SocketInterface::sendSmartPointer(const std::shared_ptr<RawMessage> &message) -> int {
    return sendByte(message.get());
}

auto SocketInterface::sendByte(const RawMessage *message) -> int {
    size_t size = message->size;
    int socket = message->socket;
    const char *source = message->message;
    char *const tooSend = new char[4];

    createHeader(static_cast<uint32_t>(size), tooSend);
    if (write(socket, tooSend, 4) <= 0) {
        valid.at(socket) = false;

        delete[] tooSend;
        return 1;
    }
    if (write(socket, source, size) <= 0) {
        valid.at(socket) = false;

        delete[] tooSend;
        return 1;
    }

    delete[] tooSend;
    return 0;
}

auto SocketInterface::receiveString(int socket) -> std::string {
    auto *recv = new RawMessage{};
    receivePtrByte(recv, socket);
    std::string return_val = std::string(recv->message, recv->size);

    delete recv->message;
    return return_val;
}

void SocketInterface::receiveSmartPtrByte(const std::shared_ptr<RawMessage> &message, int socket) {
    receivePtrByte(message.get(), socket);
}

void SocketInterface::receivePtrByte(RawMessage *const rawMessage, int socket) {

    char *const tempbuffer = new char[BUFFERSIZE];
    auto bytesReaden = static_cast<size_t>(recv(socket, tempbuffer, BUFFERSIZE, 0));

    if (bytesReaden <= 0) {
        valid.at(socket) = false;  // check if connection is still valid SIGPIPE
        delete[] tempbuffer;
        return;
    }

    uint32_t expected_size = unpackHeader(tempbuffer);
    size_t i;
    if (expected_size <= bytesReaden) {
        i = 0;
    } else {
        i = expected_size - bytesReaden;
    }

    char *const total = new char[expected_size - 4];
    unsigned timeoutTimer = TIMEOUT;

    std::memcpy(total, tempbuffer + 4,
                std::min(expected_size, (unsigned) bytesReaden) - 4);

    while (i > 4 or timeoutTimer == 0) {
        bytesReaden = static_cast<size_t>(read(
                socket, total + (expected_size - i - 4), i + 4));
        if (bytesReaden <= 0) {
            valid.at(socket) = false;
            delete[] tempbuffer;
            return;
        }

        i -= bytesReaden;

        timeoutTimer--;
    }

    delete rawMessage->message;
    delete[] tempbuffer;
    rawMessage->message = total;
    rawMessage->size = expected_size - 4;
}


void SocketInterface::closeSocket(int inputSocket) {
    if (valid.find(inputSocket) != valid.end() and valid.at(inputSocket)) {
        close(inputSocket);
        valid[inputSocket] = false;
    }
}

auto SocketInterface::checkValidity(int socket) -> bool {
    if (valid.find(socket) != valid.end()) {
        return valid.at(socket);
    }
    return false;
}

auto SocketInterface::getSocket() -> int {
    return thisSocket;
}