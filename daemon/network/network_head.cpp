//
// Created by revol-xut on 12/8/19.
//

#include "network_head.hpp"
#include "../globals.hpp"

NetworkHead::NetworkHead() {

    if (runtime->getNetworkConfig()->getBool("enable_ipc")) {
        serverIpc = std::make_shared<ServerContainer>(true);
        ipc = true;
    }
    if (runtime->getNetworkConfig()->getBool("enable_ipc")) {
        serverPub = std::make_shared<ServerContainer>(false);
        pub = true;
    }
    if (runtime->getNetworkConfig()->getBool("enable_cli")) {
        clientPub = std::make_shared<ClientContainer>();
        cli = true;
        autoConnect();
    }
}

NetworkHead::NetworkHead(bool ipc, bool pub, bool clients) {
    this->ipc = ipc, this->pub = pub, this->cli = clients;
    if (ipc) {
        serverIpc = std::make_shared<ServerContainer>(true);
    }
    if (pub) {
        serverPub = std::make_shared<ServerContainer>(false);
    }
    if (clients) {
        clientPub = std::make_shared<ClientContainer>();
        autoConnect();
    }
}

NetworkHead::~NetworkHead() = default;

void NetworkHead::checkEveryThing(const std::shared_ptr<MessageList> &messages) {
    if (ipc) {
        serverIpc->checkConnections();
        serverIpc->checkReceiving(messages);
    }
    if (pub) {
        serverPub->checkConnections();
        serverPub->checkReceiving(messages);
    }
    if (cli) {
        clientPub->checkReceiving(messages);
    }
}

void NetworkHead::autoConnect() {

    if (!cli) {
        return;
    }

    int clientCount = runtime->getNetworkConfig()->getInteger("auto_connect_count");

    std::string address;
    unsigned short port;
    for (int i = 0; i < clientCount; i++) {

        address = runtime->getNetworkConfig()->getString("host" + std::to_string(i));
        port = static_cast<unsigned short>(runtime->getNetworkConfig()->getInteger("port" + std::to_string(i)));

        clientPub->connectToEngine(address, port);

    }
}

void NetworkHead::send(const std::shared_ptr<MessageList> &messages) {

    for (const std::shared_ptr<RawMessage> &tempMessage : messages->messages) {

        switch (tempMessage->origin) {
            case 0x0:
                serverIpc->routeAndSend(tempMessage);
                break;
            case 0x1:
                serverPub->routeAndSend(tempMessage);
                break;
            case 0x2:
                clientPub->routeAndSend(tempMessage);
                break;
            default:
                runtime->getLogger(ERROR) << "origin flag on RawMessage container was unknown";
                break;
        }
    }
    messages->messages.clear();
    messages->messageCount = 0;
}