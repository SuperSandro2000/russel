//
// Created by revol-xut on 11/2/19.
//

#ifndef WORKER_GLOBALS_HPP
#define WORKER_GLOBALS_HPP

#include <memory>

#include "runtime/runtime.hpp"
/*
 * This File contains all relevant global objects:
 *  Runtime for Logging and Config
 *
 */

static std::unique_ptr<Runtime> const runtime = std::make_unique<Runtime>();

#endif