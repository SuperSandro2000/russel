//
// Created by revol-xut on 12/6/19.
//

#ifndef RUSSEL_NETWORK_INTERFACE_HPP
#define RUSSEL_NETWORK_INTERFACE_HPP

#include <memory.h>

#include "../network/socket.hpp"
#include "../network/socket_handler.hpp"

#define UNKNOWNCOMMAND "Unknown Command"; //NOLINT

class NetworkInterface {
public:
    NetworkInterface();

    ~NetworkInterface();

    void executeStack(const std::shared_ptr<MessageList> &messages, const std::shared_ptr<MessageList> &response);

    auto execute(const std::shared_ptr<RawMessage> &message, const std::shared_ptr<RawMessage> &response) -> bool;

private:


};


#endif //RUSSEL_NETWORK_INTERFACE_HPP
