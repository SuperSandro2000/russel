//
// Created by worker on 12/6/19.
//

#include "network_interface.hpp"

NetworkInterface::NetworkInterface() = default;

NetworkInterface::~NetworkInterface() = default;


void NetworkInterface::executeStack(const std::shared_ptr<MessageList> &messages,
                                    const std::shared_ptr<MessageList> &response) {

    bool create_new = true;
    std::shared_ptr<RawMessage> responseMessage;

    for (const std::shared_ptr<RawMessage> &msg : messages->messages) {
        if (msg->size > 0 and msg->message != nullptr) {
            if (create_new) {
                responseMessage = std::make_shared<RawMessage>();
            }

            create_new = execute(msg, responseMessage);

            if (create_new) {
                response->messages.push_back(responseMessage);
                response->messageCount++;
            }
        }
    }
}


auto NetworkInterface::execute(const std::shared_ptr<RawMessage> &message,
                               const std::shared_ptr<RawMessage> &response) -> bool {

    char command = message->message[0];
    response->socket = message->socket;
    response->origin = message->origin;
    switch (command) {
        case 0x1: //ping
            response->message = "pong-heart-beat";
            response->size = 15;
            return true;
        case 0x2:
            break;
        case 0x3:
            break;
            //TODO: Add Command Interface
        default:
            response->message = UNKNOWNCOMMAND
            response->size = 15;
            return true;
    }
    return false;
}