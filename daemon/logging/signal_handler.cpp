//
// Created by revol-xut on 11/1/19.
//

/*
 * This is the file which contains all signal handlers which log the signal
 */

#include "../globals.hpp"

#include <cstring>
#include <csignal>

void signalHandler(int sig) {

    std::string err;

    switch (sig) {
        case SIGHUP:
            // Signal hang up signals a too reload configuration
            runtime->getConfig()->reload();
            runtime->getNetworkConfig()->reload();
            break;

        case SIGTERM:
            // Termination Request
            runtime->terminate();
            break;
        default:
            break;
    }

    err = strsignal(sig);

    runtime->getLogger(DEBUG) << "Caught Signal: " + err;

}

