//
// Created by revol-xut on 11/1/19.
//

#include "logger.hpp"

internal_essential::Logger::Logger(const std::string &file, const std::string &status) {
    this->path = file;
    this->status = status;
    this->useSysLog = true;
    this->loggingThreshold = 2;
    this->loggingLevel = 1;
}

internal_essential::Logger::Logger() {
    this->status = "INFO";
    this->path = "";
    this->useSysLog = true;
    this->loggingThreshold = 2;
    this->loggingLevel = 1;
}

internal_essential::Logger::~Logger() {
    logFile.close();
}

void internal_essential::Logger::operator<<(const std::string &message) {
    // Maybe Change is you know how to write the stream buffer
    if (loggingLevel >= loggingThreshold) {
        setLoggingString();
        logFile.open(path, std::ios::app);
        printTime();
        logFile << ": " << status << " : " << message << "\n";
        logFile.close();
    }
}

void internal_essential::Logger::printTime() {
    constexpr int yearUnix = 1900;

    if (logFile.is_open()) {
        time_t now = time(nullptr);

        tm *ltm = localtime(&now);

        logFile << ltm->tm_mday << ".";
        logFile << 1 + ltm->tm_mon << ".";
        logFile << yearUnix + ltm->tm_year << " ";
        logFile << 1 + ltm->tm_hour << ":";
        logFile << 1 + ltm->tm_min << ":";
        logFile << 1 + ltm->tm_sec << " ";
    }
}

void internal_essential::Logger::setLoggingString() {
    switch (loggingLevel) {
        case 1:
            status = "INFO";
            return;
        case 2:
            status = "DEBUG";
            return;
        case 3:
            status = "ERROR";
            return;
        case 4:
            status = "FATAL";
            return;
        default:
            status = "DEBUG";
            return;
    }
}