//
// Created by revol-xut on 11/1/19.
//

#ifndef WORKER_LOGGER_HPP
#define WORKER_LOGGER_HPP

#include <chrono>
#include <ctime>
#include <fstream>
#include <string>
#include <utility>

namespace internal_essential {
    class Logger {
    public:
        /*!
         * @brief Creates Logger instance which provides easy to use logging files.
         * @param file Path too logfile absolute path is always better
         * @param status Priority Level (INFO, DEBUG, ERROR)
         */
        Logger(const std::string &file, const std::string &status);

        /*!
         * @brief Empty constructor file and status have to be set later
         */
        Logger();

        /*!
         * @brief destructor
         */
        ~Logger();

        /*!
         * @brief Writes given text into logfile with a timestamp and the status
         * @param message text which will be written into the logfile
         */
        void operator<<(const std::string &message);

        /*!
         * @brief Sets file path
         * @param file path to logfile
         */
        void setFile(const std::string &file) {
            this->path = file;
        }

        /*!
         * @brief Sets status of this logger
         * @param logStatus status of logger
         */
        void setStatus(const std::string &logStatus) {
            this->status = logStatus;
        }

        /*!
         * @brief Sets status of this logger
         * @param logStatus Logging Level
         */
        void setStatus(int logStatus) {
            loggingLevel = logStatus;
        }

        /*!
         * @brief Returns current logging level
         * @return logging Level
         */
        auto getIntStatus() -> int {
            return loggingLevel;
        }

        /*!
         * @brief Sets logger threshold every event below this will be ignored
         */
        void setThreshold(int threshold) {
            loggingThreshold = threshold;
        }

        /*!
         * @brief Returns logging status of this current logger object
         * @return status
         */
        auto getStatus() -> const std::string & {
            return this->status;
        }

    protected:
        /*!
         * @brief Writes timestamp
         */
        void printTime();

        /*!
         * @brief Checks for loggingLevel and sets the corresponding string e.g 1 -> "INFO", 2->"DEBUG" ...
         */
        void setLoggingString();

        int loggingLevel, loggingThreshold;
        bool useSysLog;
        std::ofstream logFile;
        std::string status, path;
    };
} // namespace internal_essential


#endif //WORKER_LOGGER_HPP
