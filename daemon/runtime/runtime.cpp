//
// Created by revol-xut on 11/1/19.
//

#include "runtime.hpp"

Runtime::Runtime() {

    logger.setFile(DEFAULT_LOG);
    logger.setStatus("INFO");

    config = std::make_shared<internal_essential::Config>(DEFAULT_CONFIG);

    networkConfig = std::make_shared<internal_essential::NetworkConfig>(config->getString("networkConf"));
    running = true;
}

Runtime::Runtime(const std::string &pathConfig, const std::string &pathLogger) {
    logger.setFile(pathLogger);
    logger.setStatus("INFO");

    config = std::make_shared<internal_essential::Config>(pathConfig);
    networkConfig = std::make_shared<internal_essential::NetworkConfig>(config->getString("networkConf"));
    running = true;
}

Runtime::~Runtime() = default;

auto Runtime::getLogger(int level) -> internal_essential::Logger & {
    logger.setStatus(level);
    return logger;
}

auto Runtime::getConfig() -> std::shared_ptr<internal_essential::Config> & {
    return config;
}

auto Runtime::getNetworkConfig() -> std::shared_ptr<internal_essential::NetworkConfig> & {
    return networkConfig;
}

auto Runtime::getRunning() -> bool {
    return running;
}

void Runtime::terminate() {
    running = false;
}
