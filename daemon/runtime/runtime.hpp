//
// Created by revol-xut on 11/1/19.
//

#ifndef WORKER_RUNTIME_HPP
#define WORKER_RUNTIME_HPP

#include <memory>

#include "../logging/logger.hpp"
#include "../config/config.hpp"
#include "../config/network_conf.hpp"

//Default Config and Logging Files
#define DEFAULT_CONFIG "/home/worker/data/default.conf" //NOLINT
#define DEFAULT_LOG "/home/worker/data/russel.log" //NOLINT

//Default Logging Levels
constexpr int FATAL = 4;
constexpr int ERROR = 3;
constexpr int DEBUG = 2;
constexpr int INFO = 1;

class Runtime {
public:
    /*!
     * @brief Default Constructor takes defined default config file paths
     */
    Runtime();

    /*!
     * @brief Creates runtime object with given paths to process relevant files like logging and configuration
     * @param pathConfig Config File
     * @param pathLogger Logging File
     */
    Runtime(const std::string &pathConfig, const std::string &pathLogger);


    ~Runtime();

    /*!
     * @brief Returns Logger for corresponding logging level
     * @param level 4 - 1 its recommended to take defined Macros
     * @return Logger reference
     */
    auto getLogger(int level) -> internal_essential::Logger &;

    /*!
     * @brief Returns default Config object
     * @return Config object
     */
    auto getConfig() -> std::shared_ptr<internal_essential::Config> &;

    /*!
     * @brief Returns the Config object which contains network information
     * @return Config object
     */
    auto getNetworkConfig() -> std::shared_ptr<internal_essential::NetworkConfig> &;

    /*!
     * @brief Gets status of daemon so it can swiftly exit
     * @return Returns if the the daemon should be terminated
     */
    auto getRunning() -> bool;

    /*!
     * @brief Starts the exiting process
     */
    void terminate();


private:
    // General Config
    std::shared_ptr<internal_essential::Config> config;
    // Network Config
    std::shared_ptr<internal_essential::NetworkConfig> networkConfig;

    internal_essential::Logger logger;
    bool running;
};


#endif //WORKER_RUNTIME_HPP
