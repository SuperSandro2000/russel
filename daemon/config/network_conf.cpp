//
// Created by revol-xut on 12/8/19.
//

#include "network_conf.hpp"

internal_essential::NetworkConfig::NetworkConfig(const std::string &path) {
    config = {};
    setDefaultValues();
    configPath = path;
}

void internal_essential::NetworkConfig::setDefaultValues() {

    // Which communication ways are enabled
    config["enable_ipc"] = "True";
    config["enable_pub"] = "True";
    config["enable_cli"] = "True";

    // Public Socket Server
    config["host"] = "127.0.0.1";
    config["port"] = "8321";

    // IPC Socket Server
    config["ipc"] = "/home/worker/data/russel.sock";

    // Auto-Connect feature
    config["auto_connect_count"] = "0";

    /*
     * The Syntax for adding new clients is:
     *  address + i         -> i is an iterator
     *  port + i
     *  Dont forget too increase "auto_connect_count" by one
     */

}

internal_essential::NetworkConfig::~NetworkConfig() = default;
