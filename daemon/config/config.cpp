//
// Created by revol-xut on 11/1/19.
//

#include "config.hpp"
#include "../globals.hpp"

internal_essential::Config::Config() {
    config = {};
    setDefaultValues();
    configPath = "";
}

internal_essential::Config::Config(const std::string &path) {
    config = {};
    setDefaultValues();
    configPath = path;
    reload();
}

internal_essential::Config::Config(const std::string &path, bool load) {
    config = {};
    configPath = path;
    if (load) {
        setDefaultValues();
    }
    reload();
}

internal_essential::Config::~Config() = default;

void internal_essential::Config::setDefaultValues() {
    //TODO: Set all default Configs
    // Network Config
    config["networkConf"] = "/home/worker/data/network.conf";

    // Distribution Engine
    config["maxTasks"] = "4";

    config["syslog"] = "1";

}

void internal_essential::Config::readFile(const std::string &file) {

    std::ifstream streamFile;
    streamFile.open(file);

    std::string line;
    while (std::getline(streamFile, line)) {

        if (line.front() != header and line.front() != comment and line.size() > 3) {

            auto equalsIndex = static_cast<unsigned int>(line.find('='));
            config[line.substr(0, equalsIndex)] = line.substr(equalsIndex + 1, line.size() - equalsIndex);
        }
    }
}

auto internal_essential::Config::getString(const std::string &key) -> std::string {
    if (config.find(key) != config.end()) {
        return config.at(key);
    }
    runtime->getLogger(ERROR) << "Key: " + key + " not found. Please Report the Bug immediately to a maintainer";
    return "";
}

auto internal_essential::Config::getInteger(const std::string &key) -> int {
    if (config.find(key) != config.end()) {
        return std::stoi(config.at(key));
    }
    runtime->getLogger(ERROR) << "Key: " + key + " not found. Please Report the Bug immediately to a maintainer";
    return -1;
}

auto internal_essential::Config::getBool(const std::string &key) -> bool {
    if (config.find(key) != config.end()) {
        const std::string &temp = config.at(key);

        return (temp == "True" or temp == "true" or temp == "1");
    }
    runtime->getLogger(ERROR) << "Key: " + key + " not found. Please Report the Bug immediately to a maintainer";
    return false;
}

void internal_essential::Config::reload() {
    if (!configPath.empty()) {
        readFile(configPath);
    } else {
        runtime->getLogger(DEBUG) << "Wants to reload conf but path is empty";
    }
}