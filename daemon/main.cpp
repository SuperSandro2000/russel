//
// Created by revol-xut on 10/31/19.
//

#include <csignal>
#include <cstdlib>
#include <sys/stat.h>
#include <syslog.h>
#include <unistd.h>

#include "config/config.hpp"
#include "globals.hpp"
#include "interface/network_interface.hpp"
#include "logging/signal_handler.hpp"
#include "network/network_head.hpp"
#include "runtime/runtime.hpp"


/*!
 * @brief Forks off Child Process which will be the daemon process the parent process is terminated
 */
static void skeleton_daemon() {
    pid_t pid;

    /* Fork off the parent process */
    pid = fork();

    /* An error occurred */
    if (pid < 0) {
        exit(EXIT_FAILURE);
    }


    /* Success: Let the parent terminate */
    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }


    /* On success: The child process becomes session leader */
    if (setsid() < 0) {
        exit(EXIT_FAILURE);
    }

    //TODO:  Write Signal specific handler */
    signal(SIGCHLD, signalHandler);
    signal(SIGHUP, signalHandler);
    signal(SIGPIPE, signalHandler);

    /* Fork off for the second time*/
    pid = fork();

    /* An error occurred */
    if (pid < 0) {
        exit(EXIT_FAILURE);
    }


    /* Success: Let the parent terminate */
    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }


    /* Set new file permissions */
    umask(0);

    /* Change the working directory to the root directory */
    /* or another appropriated directory */
    chdir("/");

    /* Close all open file descriptors */
    int x;
    for (x = (int) sysconf(_SC_OPEN_MAX); x >= 0; x--) {
        close(x);
    }

    /* Open the log file */
    openlog("firstdaemon", LOG_PID, LOG_DAEMON);
}


auto main() -> int {
    skeleton_daemon();
    runtime->getLogger(INFO) << "Runtime successfully setup ...";

    NetworkHead networkHead;

    NetworkInterface networkInterface;
    auto messagesReceive = std::make_shared<MessageList>();
    auto messagesRespond = std::make_shared<MessageList>();

    while (runtime->getRunning()) {

        networkHead.checkEveryThing(messagesReceive);
        networkInterface.executeStack(messagesReceive, messagesRespond);
        networkHead.send(messagesRespond);

    }

    syslog(LOG_NOTICE, "First daemon terminated.");
    closelog();

    return EXIT_SUCCESS;
}
