//
// Created by revol-xut on 11/14/19.
//

#include <boost/test/unit_test.hpp>
#include <string>
#include "../daemon/runtime/runtime.hpp"

BOOST_AUTO_TEST_CASE(test_runtime_logger_status) { //NOLINT
    /*!
     * Tests if runtime returns correct loggers
     */
    Runtime runtime("../../tests/files/testConf.conf", "../../tests/files/emptyLogger.log");

    BOOST_CHECK(runtime.getLogger(FATAL).getIntStatus() == 4);
    BOOST_CHECK(runtime.getLogger(ERROR).getIntStatus() == 3);
    BOOST_CHECK(runtime.getLogger(DEBUG).getIntStatus() == 2);
    BOOST_CHECK(runtime.getLogger(INFO).getIntStatus() == 1);
}

BOOST_AUTO_TEST_CASE(test_runtime_shutdown) { //NOLINT
    /*
     * Checks Termination
     */
    Runtime runtime("../../tests/files/testConf.conf", "../../tests/files/emptyLogger.log");
    runtime.terminate();

    BOOST_CHECK(!runtime.getRunning());
}

BOOST_AUTO_TEST_CASE(test_runtime_config) { //NOLINT
    /*
     * Checks if the set config path is correct
     */
    Runtime runtime("../../tests/files/testConf.conf", "../../tests/files/emptyLogger.log");
    BOOST_CHECK(runtime.getConfig()->getPath() == "../../tests/files/testConf.conf");
}
