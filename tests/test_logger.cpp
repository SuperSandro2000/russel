//
// Created by revol-xut on 11/2/19.
//

#include <boost/test/unit_test.hpp>
#include <fstream>
#include <string>

#include "../daemon/logging/logger.hpp"

#define LOGGINGFILE "../../tests/files/emptyLogger.log"
#define DUMMYTEXT "Lorem ipsum dolor sit amet, animal aperiri tacimates eos an."
#define DUMMYTEXTLENGTH 60

void discardFile() {
    /*
     * Clears the content of the test file
     */
    std::ofstream stream;
    stream.open("../../tests/files/emptyLogger.log", std::ios::out | std::ios::trunc);
    stream.close();
}


BOOST_AUTO_TEST_CASE(test_logger_write) { //NOLINT

    // Destroys all content of test logging file so the output is compare able
    discardFile();

    internal_essential::Logger logger(LOGGINGFILE, "INFO");

    auto lambdaCheck = [](int level, int threshold, internal_essential::Logger &logger, bool something,
                          const std::string &status,
                          int offSet) {
        discardFile();
        logger.setThreshold(threshold);
        logger.setStatus(level);

        std::string currentTime;
        std::string expected = "";
        if (something) {
            // We do not compare the times so we leave them out
            expected = ": " + status + " : " + DUMMYTEXT;
        }

        logger << DUMMYTEXT;

        std::ifstream streamFile;
        streamFile.open(LOGGINGFILE);
        std::string tempString;
        std::getline(streamFile, tempString);

        if (!tempString.empty()) {
            tempString = tempString.substr(tempString.size() - DUMMYTEXTLENGTH - 9 - offSet,
                                           static_cast<unsigned long>(DUMMYTEXTLENGTH + 9 + offSet));
        }

        BOOST_CHECK(tempString == expected);
    };

    lambdaCheck(1, 1, logger, true, "INFO", 0);
    lambdaCheck(1, 3, logger, false, "INFO", 0);
    lambdaCheck(4, 3, logger, true, "FATAL", 1);
    lambdaCheck(2, 3, logger, false, "DEBUG", 1);
    lambdaCheck(2, 1, logger, true, "DEBUG", 1);
}