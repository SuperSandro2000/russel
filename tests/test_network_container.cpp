//
// Created by revol-xut on 12/10/19.
//

#include <boost/test/unit_test.hpp>
#include <iostream>
#include "../daemon/network/socket_handler.hpp"
#include "../daemon/network/public_server.hpp"

BOOST_AUTO_TEST_CASE(socket_watcher_seter) { //NOLINT
    /*
     * This test checks if the socket pointer is placed correctly
     */

    auto sock = std::make_shared<PublicServer>("127.0.0.1", 8432);

    SocketWatcher socketWatcher;
    socketWatcher.setSocket(sock);
    socketWatcher.setIdentity(PUBLIC);

    BOOST_CHECK(socketWatcher.getSocket() = sock);

    socketWatcher.getSocket()->closeSocket(socketWatcher.getSocket()->getSocket());

}