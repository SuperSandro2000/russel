//
// Created by revol-xut on 11/2/19.
//

#include <boost/test/unit_test.hpp>
#include <string>
#include "../daemon/config/config.hpp"
#include "../daemon/config/network_conf.hpp"

BOOST_AUTO_TEST_CASE(test_config_parsing) { //NOLINT
    /*
     * This unit tests checks if the reading and parsing function works.
     */

    internal_essential::Config config("../../tests/files/testConf.conf");

    BOOST_CHECK(config.getString("aa") == "1");
    BOOST_CHECK(config.getString("ab") == "2");
    BOOST_CHECK(config.getString("ac") == "Test");
}

BOOST_AUTO_TEST_CASE(test_config_default_normal) { //NOLINT
    /*
     * This unit test checks if all default values are loaded correctly
     */

    internal_essential::Config config;

    BOOST_CHECK(config.getString("networkConf") == "/home/worker/data/network.conf");
    //TODO : add
}


BOOST_AUTO_TEST_CASE(test_config_default_network) { //NOLINT
    /*
     * Checks the default values from the network config
     */
    internal_essential::NetworkConfig config("../../tests/files/testConf.conf");

    BOOST_CHECK(config.getString("host") == "127.0.0.1");
    BOOST_CHECK(config.getString("port") == "8321");
    BOOST_CHECK(config.getString("ipc") == "/home/worker/data/russel.sock");
    BOOST_CHECK(config.getString("auto_connect_count") == "0");
    BOOST_CHECK(config.getString("enable_ipc") == "True");
    BOOST_CHECK(config.getString("enable_pub") == "True");
    BOOST_CHECK(config.getString("enable_cli") == "True");

}

BOOST_AUTO_TEST_CASE(test_config_casting_bool) { //NOLINT

    /*
     * Checks if the cast to bool works fine
     */

    internal_essential::Config config("../../tests/files/testConf.conf");

    BOOST_CHECK(config.getBool("aa"));
    BOOST_CHECK(config.getBool("ad"));
    BOOST_CHECK(!config.getBool("ab"));
    BOOST_CHECK(!config.getBool("ac"));
}

BOOST_AUTO_TEST_CASE(test_config_casting_int) { //NOLINT
    /*
     * Checks if the cast to bool works fine
     */

    internal_essential::Config config("../../tests/files/testConf.conf");
    BOOST_CHECK(config.getInteger("aa") == 1);
    BOOST_CHECK(config.getInteger("ab") == 2);
}